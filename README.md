Description
===========

The distance measurements are retrieved using the [SPANK ExtIO library][1], 
which internally query the micro-controller (running the [SPANK protocol][2]
embedded in the [Redskin firmware][3].

According to the hardware setup, communication with the device can be 
done using:
* I2C (requires the [bitters library][4])
* USB (requires the `libusb` library)



Supported platform
==================

LibUSB
------

All UNIX platform with libusb.


I2C
---

According to your platform I2C and GPIO configuration, you need to define 
`SPANK_EXTIO_I2C` (required) and `SPANK_EXTIO_IRQ` (if supported)
for the [bitters library][4].

If you don't have an IRQ line connected for the data-ready, you must
build for spin-wait by defining `SPANK_EXTIO_WAIT_SPINNING`, that's
the case if the `SPANK_EXTIO_IRQ` is not supported on your platform.

Some common platforms are pre-defined, using the `SPANK_EXTIO_PLATFORM`
constant:

| Platform                                                 | `SPANK_EXTIO_PLATFORM` value     |
|----------------------------------------------------------|----------------------------------|
| [Raspberry PI](https://www.raspberrypi.org/)             | `SPANK_EXTIO_PLATFORM_RPI`       |
| [PX4 Vision](http://www.holybro.com/product/px4-vision/) | `SPANK_EXTIO_PLATFORM_PX4VISION` | 


### Raspberry PI

In `/boot/config.txt` i2c peripheric is enabled with `i2c_arm=on` and
the i2c baudrate optionally configured with `i2c_arm_baudrate=xxxxx`.

For example:
~~~
dtparam=i2c_arm=on,i2c_arm_baudrate=100000
~~~


### PX4 Vision

No GPIO pin are available in the connectors, so it is required
to use spin-wait (`SPANK_EXTIO_WAIT_SPINNING`) instead of interruption.



Compilation
===========

~~~sh
git clone --recurse-submodules              # import repositories
git submodule update --init --recursive     # if --recurse-submodules forgotten
sh build.sh                                 # build program
./spank-extio-demo                          # run the demo
./spank-extio-sniffer                       # run the sniffer (only for USB)
~~~

Some compilation examples for various interfaces and platforms:
~~~sh
# I2C interface on Raspberry PI
./build.sh -DSPANK_EXTIO_FLAVOR_I2C_BITTERS                      \
           -DSPANK_EXTIO_PLATFORM=SPANK_EXTIO_PLATFORM_RPI

# I2C interface on PX4 Vision
./build.sh -DSPANK_EXTIO_FLAVOR_I2C_BITTERS                      \
           -DSPANK_EXTIO_PLATFORM=SPANK_EXTIO_PLATFORM_PX4VISION \
           -DSPANK_EXTIO_WAIT_SPINNING=5000

# I2C interface with explicitly specfied I2C and IRQ
./build.sh -DSPANK_EXTIO_FLAVOR_I2C_BITTERS                      \
           -DSPANK_EXTIO_I2C='BITTERS_I2C_INITIALIZER(1)'        \
           -DSPANK_EXTIO_IRQ='BITTERS_GPIO_PIN_INITIALIZER("gpiochip0", 22)'

# USB interface
./build.sh -DSPANK_EXTIO_FLAVOR_LIBUSB
~~~

Printing the OS clock timestamp can be achieve by passing 
`-DDEMO_WITH_TIMESTAMP` (on FreeBSD `CLOCK_MONOTONIC_PRECISE` is used,
on Linux `CLOCK_MONOTONIC_RAW`)

Extra log information, for debugging only as it could make the communication
with the device unstable, can be enabled by passing:
`-DSPANK_EXTIO_WITH_LOG`, `-DSPANK_EXTIO_LIBUSB_DEBUG`,
`-DBITTERS_I2C_WITH_LOG`, `-DBITTERS_GPIO_WITH_LOG`

See [SPANK ExtIO][1] for more informations.


Usage
=====

Run the demo
~~~sh
./spank-extio-demo
./spank-extio-demo <usec>  # usec = micro-seconds random delay for posture injection
~~~~

Run the sniffer (only available when compiling for USB)
~~~sh
./spank-extio-sniffer
~~~~



[1]: https://gitlab.inria.fr/dalu/spank-extio
[2]: https://gitlab.inria.fr/dalu/spank
[3]: https://gitlab.inria.fr/dalu/zephyr-redskin
[4]: https://gitlab.inria.fr/dalu/bitters

