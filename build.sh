#!/bin/sh

bitters=bitters
spank_extio=spank-extio/spank-extio

if [ -z "${CC}" ]; then
    for cc in cc gcc ; do
	if which ${cc} > /dev/null; then
	    CC=${cc}
	fi
    done
fi

VERSION="unknown"

if which git > /dev/null; then
    git_count=`git rev-list HEAD --count`
    git_short=`git rev-parse --short HEAD`
    git_abbrev=`git rev-parse --abbrev-ref HEAD`
    git_tag=`git describe --exact-match --abbrev=0 --tags 2> /dev/null`

    if [ -n "${git_tag}" ]; then
	VERSION=${git_tag}
    else
	VERSION="${git_abbrev}-${git_count} (${git_short})"
    fi
fi


args=$*
build_flag_enabled() {
    ${CC} -E $args - <<EOT | grep ENABLED > /dev/null
#if defined($1)
ENABLED
#endif
EOT
}


if build_flag_enabled SPANK_EXTIO_FLAVOR_I2C_BITTERS ; then
    ${CC} $* src/demo.c src/posture-helpers.c			\
	  -I include 						\
	  -D_GNU_SOURCE						\
	  ${bitters}/src/*.c					\
	  -I ${bitters}/include					\
	  -DBITTERS_I2C_WITH_ASSERT    				\
	  -DBITTERS_GPIO_WITH_ASSERT				\
	  -DBITTERS_WITH_GPIO_IRQ				\
	  -DBITTERS_WITH_THREADS				\
	  -DBITTERS_SILENCE_RPI_WARNING				\
	  ${spank_extio}/src/i2c-bitters.c			\
	  -I ${spank_extio}/include				\
	  -DVERSION="\"${VERSION}\""				\
	  -pthread						\
	  -o spank-extio-demo

elif build_flag_enabled SPANK_EXTIO_FLAVOR_LIBUSB ; then
    case `uname -s` in
	Linux)   libusb=usb-1.0 ;;
	Darwin)  libusb=usb-1.0 ;;
	FreeBSD) libusb=usb     ;;
	*)       libusb=usb     ;;
    esac

    ${CC} $* src/demo.c src/posture-helpers.c			\
	  -I include 						\
	  ${spank_extio}/src/libusb.c				\
	  -I ${spank_extio}/include				\
	  -DVERSION="\"${VERSION}\""				\
	  -pthread						\
	  -l${libusb}						\
	  -o spank-extio-demo

    ${CC} $* src/sniffer.c					\
	  -I include 						\
	  ${spank_extio}/src/libusb.c				\
	  -I ${spank_extio}/include				\
	  -DVERSION="\"${VERSION}\""				\
	  -l${libusb}						\
	  -o spank-extio-sniffer
else
cat <<EOF
Need to specify SPANK ExtIO flavor!
==> See README.md for full details

Common examples:

# I2C interface with explicitly specfied I2C and IRQ
./build.sh -DSPANK_EXTIO_FLAVOR_I2C_BITTERS                      \\
           -DSPANK_EXTIO_I2C='BITTERS_I2C_INITIALIZER(1)'        \\
           -DSPANK_EXTIO_IRQ='BITTERS_GPIO_PIN_INITIALIZER("gpiochip0", 22)'

# USB interface
./build.sh -DSPANK_EXTIO_FLAVOR_LIBUSB

EOF
    
fi

