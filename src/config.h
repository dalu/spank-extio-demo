#ifndef __CONFIG__H
#define __CONFIG__H

#include <stdlib.h>
#include <string.h>
#include <errno.h>


#define EXIT_OK		0
#define EXIT_USAGE	1
#define EXIT_ERROR	2


#define INFO(x, ...)					\
    fprintf(stdout, x "\n", ##__VA_ARGS__)

#define WARN(x, ...)					\
    fprintf(stderr, x "\n", ##__VA_ARGS__)

#define DIE(x, ...)					\
    do {						\
	fprintf(stderr, x "\n", ##__VA_ARGS__);		\
	exit(EXIT_ERROR);				\
    } while(0)

#define WARN_ERRNO(x, ...)				\
    WARN(x " (%s)", ##__VA_ARGS__, strerror(errno))

#define DIE_ERRNO(x, ...)				\
    DIE(x " (%s)", ##__VA_ARGS__, strerror(errno))

#define WARN_RC(rc, x, ...)				\
    WARN(x " (%s)", ##__VA_ARGS__, strerror(-rc))

#define DIE_RC(rc, x, ...)				\
    DIE(x " (%s)", ##__VA_ARGS__, strerror(-rc))

#endif
