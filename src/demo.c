/*
 * Copyright (c) 2020-2021
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/* Description 
 * ===========
 *
 * Test ExtIO communication.
 *
 *
 * Usage
 * -----
 *
 * spank-extio-demo [<usec>]
 *   usec = micro-seconds random delay for posture injection
 *
 *
 */


#include <pthread.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#if defined(DEMO_WITH_TIMESTAMP)
#include <time.h>
#endif

#if defined(SPANK_EXTIO_FLAVOR_I2C_BITTERS)
#include <bitters.h>
#endif

#include <spank/extio.h>

#include "config.h"
#include "posture-helpers.h"


#define SPANK_FMTaddr "%lx"
#define SPANK_FMT_ADDR(addr)  ((long) addr)

static struct {
    bool       enabled;
    useconds_t random_delay;
} posture_writer_config = { 0 };


static pthread_t posture_writer;
static void *posture_writer_task(void* parameters) {
    int rc = 0;

    while(1) {
	spank_extio_posture_t posture = {
            .position.value  = { .x = random() % 100000,
				 .y = random() % 100000,
				 .z = random() % 100000,    },
	    .velocity.value  = { .x = random() % 1000,
				 .y = random() % 1000,
				 .z = random() % 1000,      },
	    .position.stddev = SPANK_EXTIO_STDDEV_XYZ_NAN,
	    .velocity.stddev = SPANK_EXTIO_STDDEV_XYZ_NAN,
	};

	useconds_t usec = random() % (posture_writer_config.random_delay);
	printf("Injecting posture in %d µsec\n", usec);
	usleep(usec);
	if ((rc = spank_extio_set_posture(&posture)) < 0) {
	    WARN_RC(rc, "unable to set posture");
	} else {
	    char posture_str[POSTURE_DUMP_MAXLEN];
	    posture_dump(&posture, posture_str, sizeof(posture_str));
	    printf("Injected posture %s\n", posture_str);
	}
    }
    
}

int main(int argc, const char* argv[]) {
    char                  posture_str[POSTURE_DUMP_MAXLEN];
    int                   rc      = -EINVAL;
    spank_extio_posture_t posture = {
        .position.value  = { .x = random() % 100000,
			     .y = random() % 100000,
			     .z = random() % 100000,    },
	.velocity.value  = { .x = random() % 1000,
			     .y = random() % 1000,
			     .z = random() % 1000,      },
	.position.stddev = SPANK_EXTIO_STDDEV_XYZ_NAN,
	.velocity.stddev = SPANK_EXTIO_STDDEV_XYZ_NAN,
    };
 
    /* Parse arguments
     */
    if (argc >= 2) {
	const char *str    = argv[1];
	char       *endptr = NULL;
	long        val    = strtol(str, &endptr, 10);
	if ((endptr[0] != '\0') || (str[0] == '\0')) {
	    DIE("Invalid argument: number expected");
	}
	posture_writer_config.enabled = val > 0;
	posture_writer_config.random_delay = val;
	printf("Posture injection enabled with a %ld µsec random delay\n", val);
    }
    
#if defined(SPANK_EXTIO_FLAVOR_I2C_BITTERS)
    /* Initialize bitters library
     */ 
    if ((rc = bitters_init()) < 0) {
	DIE_RC(rc, "bitters library initialisation failed");
    }

    /* Try to tune program for reduced lattency
     */
    if ((rc = bitters_reduced_lattency()) < 0) {
	WARN_RC(rc, "unable to tune program for reduced latency");
    }
#endif
    
    /* Initialize SPANK ExtIO */
    if ((rc = spank_extio_init()) < 0) {
	DIE_RC(rc, "ExtIO initialisation failed");
    }

    /* Print information about subsystem
     */
    spank_extio_system_info_t sysinfo = { 0 };
    if ((rc = spank_extio_get_system_info(&sysinfo)) < 0) {
	WARN_RC(rc, "unable to get system information");
    } else {
	printf("The subsystem is using the following version:\n");
	printf(" - ExtIO   = %s\n", spank_extio_flavor());
	printf(" - Redskin = %s\n", sysinfo.version.application);
	printf(" - SPANK   = %s\n", sysinfo.version.spank);
    }

    /* Set initial posture 
     */
    if ((rc = spank_extio_set_posture(&posture)) < 0) {
	WARN_RC(rc, "unable to set posture");
    }
    posture_dump(&posture, posture_str, sizeof(posture_str));
    printf("Initial posture set to %s\n", posture_str);

    /* Small pause
     */
    printf("Pausing for ");
    for (int i = 5 ; i > 0 ; i--) {
	printf("%d...", i);
	fflush(stdout);
	sleep(1);
    }
    printf("done.\n");
    
    /* Start ExtIO notifcations
     */
    if ((rc = spank_extio_start(SPANK_EXTIO_EVENT_DISTANCE_MEASUREMENT,
				SPANK_EXTIO_BEHAVIOUR_DROP_DEFAULT)) < 0) {
	WARN_RC(rc, "unable to start ExtIO");
    }
    
    /* Start posture writer if requested
     */
    if (posture_writer_config.enabled) {
	if ((rc = pthread_create(&posture_writer,
				 NULL, posture_writer_task, NULL) < 0)) {
	    DIE("unable to create thread: %s", strerror(rc));
	}
    }

    
    /* Loop against received events
     * NOTE: On RaspberryPi (under Linux) due to BCM2835 hardware/driver
     *       limitation it is necessary to pass NULL as info parameter
     *       for spank_extio_fetch_distance_measurement()
     */
    printf("Start looping...\n");
    while(1) {
	/* Wait on events.
	 * List of wanted events has been registered in spank_extio_start()
	 */
	int events = spank_extio_wait(1000);
	if (events < 0) {
	    switch(-events) {
	    case EAGAIN:
		// That's kind of expected with the way interrupt processing
		// is done in spank_extio_wait()
		WARN("restarting wait");
		break;
	    case ETIMEDOUT:
		WARN("wait timed out");
		break;
	    default:
		WARN_RC(events, "failed waiting for ExtIO events");
		break;
	    }
	    continue;
	}

	/* Process *all* distance measurements
	 */
	if (events & SPANK_EXTIO_EVENT_DISTANCE_MEASUREMENT) {
	    // Use new variable 'next_events' to not pollute
	    // 'events' in case of error
	    int next_events = SPANK_EXTIO_EVENT_NONE;

	    do {
		// Retrieve distance measurement
		spank_extio_distance_measurement_t       dm    = { 0 };
		spank_extio_info_t                       info  = { 0 };

		// Extra has only been implemented yet for extio-usb
#if defined(SPANK_EXTIO_FLAVOR_LIBUSB)
		spank_extio_distance_measurement_extra_t extra = { 0 };
		rc = spank_extio_fetch_distance_measurement(&dm, &extra, NULL);
#else
		rc = spank_extio_fetch_distance_measurement(&dm, NULL, NULL);
#endif
#if defined(DEMO_WITH_TIMESTAMP)
		    struct timespec time;
#if defined(__linux__)
		    clockid_t clockid  = CLOCK_MONOTONIC_RAW;
		    char     *clockstr = "monotonic-raw";
#else
		    clockid_t clockid  = CLOCK_MONOTONIC_PRECISE;
		    char     *clockstr = "monotonic-precise";
#endif
		    clock_gettime(clockid, &time);
		    printf("OS Clock = %ld.%09ld (%s)\n",
			   time.tv_sec, time.tv_nsec, clockstr);
#endif		    
		    
		if (rc < 0) {
		    WARN_RC(rc, "failed to fetch distance measurement");
		} else {
		    printf("Distance = %4d"
			   " [" SPANK_FMTaddr " -> " "]"
			   " <%4d, %4d, %4d>"
			   " (%d ms)"
			   "\n",
			   dm.distance,
			   SPANK_FMT_ADDR(dm.id),
			   dm.posture.position.value.x,
			   dm.posture.position.value.y,
			   dm.posture.position.value.z,
			   dm.freshness
			   );
#if defined(SPANK_EXTIO_FLAVOR_LIBUSB)
		    printf("   Power = %0.2f (%0.2f)\n"
			   "   Clock = %d/%d\n"
			   "   Chip  = T: %0.2f, V: %0.3f\n",
			   extra.power.firstpath / 100.0,
			   extra.power.signal    / 100.0,
			   extra.clock_tracking.offset,
			   extra.clock_tracking.interval,
			   extra.chip_info.temp / 100.0,
			   extra.chip_info.vbat / 1000.0);
#endif			   
		}

		// Look for other pending distance measurements
		if ((next_events = spank_extio_events_ready()) < 0) {
		    WARN_RC(next_events, "failed to read events ready");
		}

	    } while((next_events > 0) &&
		    (next_events & SPANK_EXTIO_EVENT_DISTANCE_MEASUREMENT));
	}
    }
    
    /* Job's done...
     *  (it's a lie, we never finish)
     */
    return 0;
}



/* 
 * Local Variables:
 * c-basic-offset: 4
 * End:
 */
