/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "posture-helpers.h"


static bool
posture_scan_xyz(char *str, int *x, int *y, int *z)
{
    int scan_end;
    int s = sscanf(str, "%d,%d,%d%n", x, y, z, &scan_end);
    return (s == 3) && (str[scan_end] == '\0');
}

bool
posture_scan_position(char *str, spank_extio_position_t *v)
{
    int x, y, z;
    if      (!strcmp(str, "0")) { spank_extio_position_set_zero(v);
	                          return true;  			}
    else if (!strcmp(str, "-")) { spank_extio_position_set_nan(v);
	                          return true;  			}
    else if (!strcmp(str, ".")) { return true;  			}
    else if (!posture_scan_xyz(str, &x, &y, &z)) { return false;	}
    else                        { spank_extio_position_from_int(v, x, y, z);
	                          return true;  			}
}

bool
posture_scan_velocity(char *str, spank_extio_velocity_t *v)
{
    int x, y, z;
    if      (!strcmp(str, "0")) { spank_extio_velocity_set_zero(v);
	                          return true;  			}
    else if (!strcmp(str, "-")) { spank_extio_velocity_set_nan(v);
	                          return true;  			}
    else if (!strcmp(str, ".")) { return true;				}
    else if (!posture_scan_xyz(str, &x, &y, &z)) { return false;	}
    else                        { spank_extio_velocity_from_int(v, x, y, z);
	                          return true; 				}
}

bool
posture_scan_stddev_xyz(char *str, spank_extio_stddev_xyz_t *v)
{
    int x, y, z;
    if      (!strcmp(str, "0")) { spank_extio_stddev_xyz_set_zero(v);
	                          return true;  			}
    else if (!strcmp(str, "-")) { spank_extio_stddev_xyz_set_nan(v);
	                          return true;  			}
    else if (!strcmp(str, ".")) { return true;  			}
    else if (!posture_scan_xyz(str, &x, &y, &z)) { return false; 	}
    else                        { spank_extio_stddev_xyz_from_int(v, x, y, z);
                                  return true; 				}
}


bool
posture_dump(spank_extio_posture_t *posture, char *bufptr, size_t buflen)
{
    int rc;
    
#define POSTURE_DUMP_SNPRINTF(args...) do {	\
	rc = snprintf(bufptr, buflen, args);	\
	if ((rc < 0) || (buflen < rc))		\
	    return false;			\
	bufptr += rc;				\
	buflen -= rc;				\
    } while(0) 

    if (SPANK_EXTIO_POSITION_IS_NAN(posture->position.value)) {
	POSTURE_DUMP_SNPRINTF("%s", "n/a");
	return true;
    }

    POSTURE_DUMP_SNPRINTF("<%"SPANK_EXTIO_PRIposition_axis","
			   "%"SPANK_EXTIO_PRIposition_axis","
			   "%"SPANK_EXTIO_PRIposition_axis">",
			  posture->position.value.x,
			  posture->position.value.y,
			  posture->position.value.z);
    
    if (SPANK_EXTIO_STDDEV_XYZ_IS_NAN(posture->position.stddev))
	goto position_done;
    
    POSTURE_DUMP_SNPRINTF("(%"SPANK_EXTIO_PRIstddev_axis","
			   "%"SPANK_EXTIO_PRIstddev_axis","
			   "%"SPANK_EXTIO_PRIstddev_axis")",
			  posture->position.stddev.x,
			  posture->position.stddev.y,
			  posture->position.stddev.z);
 position_done:
    
    if (SPANK_EXTIO_VELOCITY_IS_NAN(posture->velocity.value))
	goto velocity_done;

    POSTURE_DUMP_SNPRINTF(" " "<%"SPANK_EXTIO_PRIvelocity_axis","
			       "%"SPANK_EXTIO_PRIvelocity_axis","
			       "%"SPANK_EXTIO_PRIvelocity_axis">",
			  posture->velocity.value.x,
			  posture->velocity.value.y,
			  posture->velocity.value.z);

    if (SPANK_EXTIO_STDDEV_XYZ_IS_NAN(posture->velocity.stddev))
	goto velocity_done;
	
    POSTURE_DUMP_SNPRINTF("(%"SPANK_EXTIO_PRIstddev_axis","
			   "%"SPANK_EXTIO_PRIstddev_axis","
			   "%"SPANK_EXTIO_PRIstddev_axis")",
			  posture->velocity.stddev.x,
			  posture->velocity.stddev.y,
			  posture->velocity.stddev.z);
 velocity_done:
    
    return true;
    
#undef POSTURE_DUMP_SNPRINTF
}
