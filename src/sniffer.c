/*
 * Copyright (c) 2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/* Description 
 * ===========
 *
 * Dump frame
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#include <spank/extio.h>

#include "config.h"



int main(int argc, const char* argv[]) {
    int rc = -EINVAL;
    
    /* Initialize SPANK ExtIO */
    if ((rc = spank_extio_init()) < 0) {
	DIE_RC(rc, "ExtIO initialisation failed");
    }

    /* Print information about subsystem
     */
    spank_extio_system_info_t sysinfo = { 0 };
    if ((rc = spank_extio_get_system_info(&sysinfo)) < 0) {
	WARN_RC(rc, "unable to get system information");
    } else {
	printf("The subsystem is using the following version:\n");
	printf(" - ExtIO   = %s\n", spank_extio_flavor());
	printf(" - Redskin = %s\n", sysinfo.version.application);
	printf(" - SPANK   = %s\n", sysinfo.version.spank);
    }
    
    /* Enable sniffer
     */
    if ((rc = spank_extio_enable_frame_sniffer() < 0)) {
	WARN_RC(rc, "unable to enable ExtIO frame sniffer");
    }
    
    /* Loop
     */
    printf("Sniffer started...\n");
    while(1) {
	union {
	    struct spank_extio_dumped_frame dumped_frame;
	    uint8_t raw[SPANK_EXTIO_DUMPED_FRAME_MAXSIZE(128)];
	} data;

	if ((rc = spank_extio_frame_sniffer(data.raw, sizeof(data.raw))) < 0) {
	    WARN_RC(rc, "unable to get dumped frame");
	} else {
	    spank_extio_dumped_frame_header_t *header =
		&data.dumped_frame.header;
	    
	    printf("Frame o size          = %d\n"
		   "      | timestamp     = %lu\n"
		   "      | os ticks      = %lu\n"
		   "      | power         = %f (%f)\n"
		   "      o time-tracking = %d/%d\n",
		   header->frame_length,
		   header->timestamp,
		   header->os_ticks,
		   header->power.firstpath / 100.0,
		   header->power.signal    / 100.0,
		   header->clock_tracking.offset,
		   header->clock_tracking.interval
		   );
	}
    }
    
    /* Job's done...
     *  (it's a lie, we never finish)
     */
    return 0;
}



/* 
 * Local Variables:
 * c-basic-offset: 4
 * End:
 */
